

let trainer = {}

trainer.name = "Ash Ketchum",
trainer.age = 10,
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
trainer.friends = {
	hoenn: ["May", "Max"],
	kanto: ["Brock", "Misty"]
}
trainer.talk = function(){
	console.log("Pikachu! I choose you!");
}

console.log(trainer)

console.log("Result of dot notation: ");
console.log(trainer.name);
console.log("Rsult of square bracket notation: ");
console.log(trainer['pokemon']);
console.log("Result of talk method");
trainer.talk()


function Pokemon(name, level){

	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;

	this.tackle = function(target){

		console.log(this.name + " tackled "+target.name);

		console.log(target.name +"'s health is now reduced to "+(target.health - this.attack));

		this.newHealth = target.health - this.attack

			if(this.newHealth <= 0){
			target.faint();
		}

	}
	this.faint = function(){
		console.log(this.name+ " has fainted.");
	}

}


let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodud", 8);
console.log(geodude);
			
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu)

console.log(pikachu)

mewtwo.tackle(geodude);

console.log(geodude);